#!/bin/bash

echo "Auxílio para redirecionadores"

echo "Os redirecionadores conectam a saída do comando para a entrada do próximo comando"

echo "O > irá redirecionar a sáida e sobrescrever o arquivo"
echo "Exemplo do >: ls /home/ifpb/Downloads > /home/ifpb/conteudo.txt"
echo "A saída do comando ls irá ser redirecionada para o arquivo conteudo.txt"
sleep 1
echo "O >> irá adicionar a saída ao final, sem sobrescrever"
echo "Exemplo do >>: ls /home/ifpb/Documentos >> /home/ifpb/conteudo.txt"
echo "Semelhante ao exemplo anterior, mas o comando irá ser redirecionado ao final do arquivo, sem retirar o conteúdo armazenado anteriormente."

sleep 1

echo "O | irá conectar a saída do comando anterior para o próximo comando"
echo "Exemplo do | : ps -ef | grep bash"

sleep 1

echo "O 2> irá redirecionar a saída do erro"
echo "Exemplo: ls sudo 2> /home/ifpb/conteudo.txt"

sleep 1

echo "O 2>> irá redirecionar a saída do erro, mas adicionando ao final, sem sobreescrever"
echo "Exemplo ls /HoMe/IFFFPBB 2>> /home/ifpb/conteudo.txt"

sleep 1

echo "O &> irá redirecionar todas as saídas, os comandos bem sucedidos e os erros"
echo "Exemplo: ls /home/ifpb/Downloads &> /home/ifpb/conteudo.txt"
echo "Exemplo 2: ls /HoMe/IFFFPBB &> /home/ifpb/conteudo.txt"

sleep 1

echo "O &>> irá redirecionar todas as saídas, mas adicionando sempre ao final, sem sobrescrever"
echo "Exemplo: ls /home/ifpb/Documentos &>> /home/ifpb/conteudo.txt"

sleep 1

echo "O < irá redirecionar para a entrada"
echo "Exemplo: cat < /home/ifpb/conteudo.txt"


