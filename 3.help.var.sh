#!/bin/bash

echo "Variavéis Automáticas"

echo '$#' "Retorna a quantidade de argumentos passados na linha de comando"
echo "Exemplo: ./teste.sh Shell Script Linux"
echo "Nesse exemplo ele iria indicar o número 3."

echo '$*' "Retorna quais foram os argumentos passados na linha de comando"
echo "Exemplo: ./teste.sh Shell Script Linux" 
echo "Nesse exemplo ele iria retornar Shell, Script e Linux, que foram os argumentos passados"


